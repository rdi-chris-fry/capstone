﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using CapstoneProject.Models;

namespace CapstoneProject.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult FindHelp()
        {
            var lat = Request.Cookies["latitude"];
            var lon = Request.Cookies["longitude"];

            Response.Redirect($"https://www.google.com/maps/search/addictions+treatment/@{lat},{lon},12z");

            return View("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
