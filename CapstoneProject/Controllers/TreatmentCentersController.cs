﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CapstoneProject.Models.Database;

namespace CapstoneProject.Controllers
{
    public class TreatmentCentersController : Controller
    {
        private readonly DatabaseContext _context;

        public TreatmentCentersController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: Treatment Centers
        public async Task<IActionResult> Index()
        {
            return View(await _context.TreatmentCenter.ToListAsync());
        }

        // GET: Treatment Centers/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var TreatmentCenter = await _context.TreatmentCenter
                .FirstOrDefaultAsync(m => m.Id == id);
            if (TreatmentCenter == null)
            {
                return NotFound();
            }

            return View(TreatmentCenter);
        }

        // GET: Treatment Centers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Treatment Centers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Organization,Address1,Address2,City,State,ZipCode,PhoneNumber,Email,Longitude,Latitude,Id")] TreatmentCenter TreatmentCenter)
        {
            if (ModelState.IsValid)
            {
                _context.Add(TreatmentCenter);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(TreatmentCenter);
        }

        // GET: Treatment Centers/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var TreatmentCenter = await _context.TreatmentCenter.FindAsync(id);
            if (TreatmentCenter == null)
            {
                return NotFound();
            }
            return View(TreatmentCenter);
        }

        // POST: Treatment Centers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Name,Organization,Address1,Address2,City,State,ZipCode,PhoneNumber,Email,Longitude,Latitude,Id")] TreatmentCenter TreatmentCenter)
        {
            if (id != TreatmentCenter.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(TreatmentCenter);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TreatmentCenterExists(TreatmentCenter.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(TreatmentCenter);
        }

        // GET: Treatment Centers/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var TreatmentCenter = await _context.TreatmentCenter
                .FirstOrDefaultAsync(m => m.Id == id);
            if (TreatmentCenter == null)
            {
                return NotFound();
            }

            return View(TreatmentCenter);
        }

        // POST: Treatment Centers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var TreatmentCenter = await _context.TreatmentCenter.FindAsync(id);
            _context.TreatmentCenter.Remove(TreatmentCenter);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TreatmentCenterExists(long id)
        {
            return _context.TreatmentCenter.Any(e => e.Id == id);
        }
    }
}
