﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CapstoneProject.Models.Database;

namespace CapstoneProject.Controllers
{
    public class HotlinesController : Controller
    {
        private readonly DatabaseContext _context;

        public HotlinesController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: Hotlines
        public async Task<IActionResult> Index()
        {
            return View(await _context.Hotline.ToListAsync());
        }

        // GET: Hotlines/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hotline = await _context.Hotline
                .FirstOrDefaultAsync(m => m.Id == id);
            if (hotline == null)
            {
                return NotFound();
            }

            return View(hotline);
        }

        // GET: Hotlines/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Hotlines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PhoneNumber,ChatAddress,Email,Organization,SuicidePreventionSupport,Id,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy")] Hotline hotline)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hotline);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(hotline);
        }

        // GET: Hotlines/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hotline = await _context.Hotline.FindAsync(id);
            if (hotline == null)
            {
                return NotFound();
            }
            return View(hotline);
        }

        // POST: Hotlines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("PhoneNumber,ChatAddress,Email,Organization,SuicidePreventionSupport,Id,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy")] Hotline hotline)
        {
            if (id != hotline.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(hotline);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HotlineExists(hotline.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(hotline);
        }

        // GET: Hotlines/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hotline = await _context.Hotline
                .FirstOrDefaultAsync(m => m.Id == id);
            if (hotline == null)
            {
                return NotFound();
            }

            return View(hotline);
        }

        // POST: Hotlines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var hotline = await _context.Hotline.FindAsync(id);
            _context.Hotline.Remove(hotline);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HotlineExists(long id)
        {
            return _context.Hotline.Any(e => e.Id == id);
        }
    }
}
