﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CapstoneProject.Models.Database;

namespace CapstoneProject.Controllers
{
    public class EmotionsController : Controller
    {
        private readonly DatabaseContext _context;

        public EmotionsController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: Emotions
        public async Task<IActionResult> Index()
        {
            return View(await _context.Emotion.ToListAsync());
        }

        // GET: Emotions/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var emotion = await _context.Emotion
                .FirstOrDefaultAsync(m => m.Id == id);
            if (emotion == null)
            {
                return NotFound();
            }

            return View(emotion);
        }

        // GET: Emotions/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Emotions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EmotionName,Id,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy")] Emotion emotion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(emotion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(emotion);
        }

        // GET: Emotions/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var emotion = await _context.Emotion.FindAsync(id);
            if (emotion == null)
            {
                return NotFound();
            }
            return View(emotion);
        }

        // POST: Emotions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("EmotionName,Id,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy")] Emotion emotion)
        {
            if (id != emotion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(emotion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmotionExists(emotion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(emotion);
        }

        // GET: Emotions/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var emotion = await _context.Emotion
                .FirstOrDefaultAsync(m => m.Id == id);
            if (emotion == null)
            {
                return NotFound();
            }

            return View(emotion);
        }

        // POST: Emotions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var emotion = await _context.Emotion.FindAsync(id);
            _context.Emotion.Remove(emotion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmotionExists(long id)
        {
            return _context.Emotion.Any(e => e.Id == id);
        }
    }
}
