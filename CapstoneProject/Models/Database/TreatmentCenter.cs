﻿using System.Collections.Generic;
using System.ComponentModel;

namespace CapstoneProject.Models.Database
{
    public class TreatmentCenter : Entity<long>
    {
        public string Name { get; set; }

        public bool Organization { get; set; }

        [DisplayName("Address 1")]
        public string Address1 { get; set; }

        [DisplayName("Address 2")]
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }

        [DisplayName("Zip Code")]
        public string ZipCode { get; set; }

        [DisplayName("Phone Number")]
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public virtual ICollection<Drug> Drugs { get; set; }
        public virtual ICollection<Emotion> Emotions { get; set; }
    }
}