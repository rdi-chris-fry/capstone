﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CapstoneProject.Models.Database
{
    public class HotlineConfiguration : IEntityTypeConfiguration<Hotline>
    {
        public void Configure(EntityTypeBuilder<Hotline> builder)
        {
            builder.ToTable("Hotline", "Capstone");
            builder.Property(p => p.PhoneNumber).IsRequired().HasMaxLength(100);
            builder.Property(p => p.Email).IsRequired().HasMaxLength(100);
            builder.Property(p => p.ChatAddress).HasMaxLength(300);
            builder.HasMany(p => p.Drugs).WithOne(p => p.Hotline);
            builder.HasMany(p => p.Emotions).WithOne(p => p.Hotline);
            builder.HasIndex(i => new {i.Email});
        }
    }
}
