﻿using System.Collections.Generic;
using System.ComponentModel;
using CapstoneProject.Models.Enums;

namespace CapstoneProject.Models.Database
{
    public class UserProfile : Entity<long>
    {
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Middle Name")]
        public string MiddleName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }
        public string Email { get; set; }

        [DisplayName("Phone Number")]
        public string PhoneNumber { get; set; }

        [DisplayName("Address 1")]
        public string Address1 { get; set; }

        [DisplayName("Address 2")]
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }

        [DisplayName("Zip Code")]
        public string ZipCode { get; set; }

        [DisplayName("Preferred Contact Method")]
        public ContactMethod PreferredContactMethod { get; set; }
        public virtual ICollection<Drug> Drugs { get; set; }
    }
}