﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CapstoneProject.Models.Database
{
    class UserProfileConfiguration : IEntityTypeConfiguration<UserProfile>
    {
        public void Configure(EntityTypeBuilder<UserProfile> builder)
        {
            builder.ToTable("UserProfile", "Capstone");
            builder.Property(p => p.Address1).IsRequired().HasMaxLength(100);
            builder.Property(p => p.Address2).HasMaxLength(100);
            builder.Property(p => p.City).IsRequired().HasMaxLength(100);
            builder.Property(p => p.State).IsRequired().HasMaxLength(100);
            builder.Property(p => p.ZipCode).IsRequired().HasMaxLength(10);
            builder.Property(p => p.PhoneNumber).IsRequired().HasMaxLength(100);
            builder.Property(p => p.Email).IsRequired().HasMaxLength(100);
            builder.Property(p => p.FirstName).IsRequired().HasMaxLength(100);
            builder.Property(p => p.MiddleName).HasMaxLength(100);
            builder.Property(p => p.LastName).IsRequired().HasMaxLength(100);
            builder.Property(p => p.PreferredContactMethod).IsRequired();
            builder.HasMany(p => p.Drugs).WithOne(p => p.UserProfile);
            builder.HasIndex(i => new {i.Email});
        }
    }
}