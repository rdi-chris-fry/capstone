﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CapstoneProject.Models.Database
{
    public class EmotionConfiguration : IEntityTypeConfiguration<Emotion>
    {
        public void Configure(EntityTypeBuilder<Emotion> builder)
        {
            builder.ToTable("Emotion", "Capstone");
            builder.Property(p => p.EmotionName).IsRequired().HasMaxLength(300);
            builder.HasIndex(i => new {i.EmotionName});
        }
    }
}
