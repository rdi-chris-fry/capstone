﻿using System.ComponentModel;

namespace CapstoneProject.Models.Database
{
    public class Drug : Entity<long>
    {
        [DisplayName("Drug Name")]
        public string DrugName { get; set; }

        public virtual Hotline Hotline { get; set; }
        public virtual TreatmentCenter TreatmentCenter { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}