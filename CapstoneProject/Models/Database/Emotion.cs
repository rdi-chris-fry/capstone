﻿using System.ComponentModel;

namespace CapstoneProject.Models.Database
{
    public class Emotion : Entity<long>
    {
        [DisplayName("Emotion")]
        public string EmotionName { get; set; }

        public virtual Hotline Hotline { get; set; }
        public virtual TreatmentCenter TreatmentCenter { get; set; }
    }
}