﻿using System.Collections.Generic;
using System.ComponentModel;

namespace CapstoneProject.Models.Database
{
    public class Hotline : Entity<long>
    {
        [DisplayName("Phone Number")]
        public string PhoneNumber { get; set; }

        [DisplayName("Chat Address")]
        public string ChatAddress { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Organization Name")]
        public string Organization { get; set; }

        [DisplayName("Suicide Prevention?")]
        public bool SuicidePreventionSupport { get; set; }
        public virtual ICollection<Drug> Drugs { get; set; }
        public virtual ICollection<Emotion> Emotions { get; set; }
    }
}