﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CapstoneProject.Models.Database
{
    public abstract class Entity<T> : IEntity<T>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public T Id { get; set; }
        object IEntity.Id
        {
            get { return Id; }
            set { Id = (T)value; }
        }

        private DateTime? _createdDate;
        [DataType(DataType.DateTime)]
        [Display(Name = "Creation Date")]
        public DateTime CreatedDate
        {
            get { return _createdDate ?? DateTime.UtcNow; }
            set { _createdDate = value; }
        }

        [DataType(DataType.DateTime)]
        [Display(Name = "Modification Date")]
        public DateTime ModifiedDate { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [StringLength(100)]
        [Display(Name = "Modified By")]
        public string ModifiedBy { get; set; }
    }
}