﻿using Microsoft.EntityFrameworkCore;

namespace CapstoneProject.Models.Database
{
    public class DatabaseContext : DbContext
    {
        public virtual DbSet<Drug> Drug { get; set; }
        public virtual DbSet<Emotion> Emotion { get; set; }
        public virtual DbSet<Hotline> Hotline { get; set; }
        public virtual DbSet<TreatmentCenter> TreatmentCenter { get; set; }
        public virtual DbSet<UserProfile> UserProfile { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new DrugConfiguration());
            modelBuilder.ApplyConfiguration(new EmotionConfiguration());
            modelBuilder.ApplyConfiguration(new HotlineConfiguration());
            modelBuilder.ApplyConfiguration(new TreatmentCenterConfiguration());
            modelBuilder.ApplyConfiguration(new UserProfileConfiguration());
        }
    }
}
