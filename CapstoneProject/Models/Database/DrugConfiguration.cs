﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CapstoneProject.Models.Database
{
    public class DrugConfiguration : IEntityTypeConfiguration<Drug>
    {
        public void Configure(EntityTypeBuilder<Drug> builder)
        {
            builder.ToTable("Drug", "Capstone");
            builder.Property(p => p.DrugName).IsRequired().HasMaxLength(300);
            builder.HasIndex(i => new {i.DrugName});
        }
    }
}
