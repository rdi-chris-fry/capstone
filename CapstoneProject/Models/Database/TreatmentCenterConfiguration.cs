﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CapstoneProject.Models.Database
{
    public class TreatmentCenterConfiguration : IEntityTypeConfiguration<TreatmentCenter>
    {
        public void Configure(EntityTypeBuilder<TreatmentCenter> builder)
        {
            builder.ToTable("TreatmentCenter", "Capstone");
            builder.Property(p => p.Address1).IsRequired().HasMaxLength(100);
            builder.Property(p => p.Address2).HasMaxLength(100);
            builder.Property(p => p.City).IsRequired().HasMaxLength(100);
            builder.Property(p => p.State).IsRequired().HasMaxLength(100);
            builder.Property(p => p.ZipCode).IsRequired().HasMaxLength(10);
            builder.Property(p => p.PhoneNumber).IsRequired().HasMaxLength(100);
            builder.Property(p => p.Email).IsRequired().HasMaxLength(100);
            builder.Property(p => p.Name).IsRequired().HasMaxLength(100);
            builder.HasMany(p => p.Drugs).WithOne(p => p.TreatmentCenter);
            builder.HasMany(p => p.Emotions).WithOne(p => p.TreatmentCenter);
            builder.HasIndex(i => new {i.Email});
        }
    }
}