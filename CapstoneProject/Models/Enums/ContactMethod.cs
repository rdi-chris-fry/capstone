﻿namespace CapstoneProject.Models.Enums
{
    public enum ContactMethod
    {
        Text,
        Phone,
        Email
    }
}
