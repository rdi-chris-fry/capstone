﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CapstoneProject.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Capstone");

            migrationBuilder.CreateTable(
                name: "Hotline",
                schema: "Capstone",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 100, nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 100, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 100, nullable: false),
                    ChatAddress = table.Column<string>(maxLength: 300, nullable: true),
                    Email = table.Column<string>(maxLength: 100, nullable: false),
                    Organization = table.Column<string>(nullable: true),
                    SuicidePreventionSupport = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hotline", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TreatmentCenter",
                schema: "Capstone",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 100, nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 100, nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Organization = table.Column<bool>(nullable: false),
                    Address1 = table.Column<string>(maxLength: 100, nullable: false),
                    Address2 = table.Column<string>(maxLength: 100, nullable: true),
                    City = table.Column<string>(maxLength: 100, nullable: false),
                    State = table.Column<string>(maxLength: 100, nullable: false),
                    ZipCode = table.Column<string>(maxLength: 10, nullable: false),
                    PhoneNumber = table.Column<string>(maxLength: 100, nullable: false),
                    Email = table.Column<string>(maxLength: 100, nullable: false),
                    Longitude = table.Column<double>(nullable: true),
                    Latitude = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TreatmentCenter", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserProfile",
                schema: "Capstone",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 100, nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 100, nullable: true),
                    FirstName = table.Column<string>(maxLength: 100, nullable: false),
                    MiddleName = table.Column<string>(maxLength: 100, nullable: true),
                    LastName = table.Column<string>(maxLength: 100, nullable: false),
                    Email = table.Column<string>(maxLength: 100, nullable: false),
                    PhoneNumber = table.Column<string>(maxLength: 100, nullable: false),
                    Address1 = table.Column<string>(maxLength: 100, nullable: false),
                    Address2 = table.Column<string>(maxLength: 100, nullable: true),
                    City = table.Column<string>(maxLength: 100, nullable: false),
                    State = table.Column<string>(maxLength: 100, nullable: false),
                    ZipCode = table.Column<string>(maxLength: 10, nullable: false),
                    PreferredContactMethod = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserProfile", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Emotion",
                schema: "Capstone",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 100, nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 100, nullable: true),
                    EmotionName = table.Column<string>(maxLength: 300, nullable: false),
                    HotlineId = table.Column<long>(nullable: true),
                    TreatmentCenterId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Emotion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Emotion_Hotline_HotlineId",
                        column: x => x.HotlineId,
                        principalSchema: "Capstone",
                        principalTable: "Hotline",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Emotion_TreatmentCenter_TreatmentCenterId",
                        column: x => x.TreatmentCenterId,
                        principalSchema: "Capstone",
                        principalTable: "TreatmentCenter",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Drug",
                schema: "Capstone",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 100, nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 100, nullable: true),
                    DrugName = table.Column<string>(maxLength: 300, nullable: false),
                    HotlineId = table.Column<long>(nullable: true),
                    TreatmentCenterId = table.Column<long>(nullable: true),
                    UserProfileId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drug", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Drug_Hotline_HotlineId",
                        column: x => x.HotlineId,
                        principalSchema: "Capstone",
                        principalTable: "Hotline",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Drug_TreatmentCenter_TreatmentCenterId",
                        column: x => x.TreatmentCenterId,
                        principalSchema: "Capstone",
                        principalTable: "TreatmentCenter",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Drug_UserProfile_UserProfileId",
                        column: x => x.UserProfileId,
                        principalSchema: "Capstone",
                        principalTable: "UserProfile",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Drug_DrugName",
                schema: "Capstone",
                table: "Drug",
                column: "DrugName");

            migrationBuilder.CreateIndex(
                name: "IX_Drug_HotlineId",
                schema: "Capstone",
                table: "Drug",
                column: "HotlineId");

            migrationBuilder.CreateIndex(
                name: "IX_Drug_TreatmentCenterId",
                schema: "Capstone",
                table: "Drug",
                column: "TreatmentCenterId");

            migrationBuilder.CreateIndex(
                name: "IX_Drug_UserProfileId",
                schema: "Capstone",
                table: "Drug",
                column: "UserProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_Emotion_EmotionName",
                schema: "Capstone",
                table: "Emotion",
                column: "EmotionName");

            migrationBuilder.CreateIndex(
                name: "IX_Emotion_HotlineId",
                schema: "Capstone",
                table: "Emotion",
                column: "HotlineId");

            migrationBuilder.CreateIndex(
                name: "IX_Emotion_TreatmentCenterId",
                schema: "Capstone",
                table: "Emotion",
                column: "TreatmentCenterId");

            migrationBuilder.CreateIndex(
                name: "IX_Hotline_Email",
                schema: "Capstone",
                table: "Hotline",
                column: "Email");

            migrationBuilder.CreateIndex(
                name: "IX_TreatmentCenter_Email",
                schema: "Capstone",
                table: "TreatmentCenter",
                column: "Email");

            migrationBuilder.CreateIndex(
                name: "IX_UserProfile_Email",
                schema: "Capstone",
                table: "UserProfile",
                column: "Email");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Drug",
                schema: "Capstone");

            migrationBuilder.DropTable(
                name: "Emotion",
                schema: "Capstone");

            migrationBuilder.DropTable(
                name: "UserProfile",
                schema: "Capstone");

            migrationBuilder.DropTable(
                name: "Hotline",
                schema: "Capstone");

            migrationBuilder.DropTable(
                name: "TreatmentCenter",
                schema: "Capstone");
        }
    }
}
